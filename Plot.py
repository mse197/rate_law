import matplotlib.pyplot as plt

def plotter ( wavelength, absorbance,name, x):
    plt.plot(wavelength,absorbance) #the two lists we added to are plotted here
    plt.title(('Wavelength as a Function of Absorbance {}').format(x))
    plt.xlabel('Wavelength (nm)')
    plt.ylabel('Absorbance' )
    plt.savefig( "plots/{}.png".format(name))
    plt.savefig( "plots/{}.png".format(name))

