# Create filename function to use in file organizing loop. The csv is organized with the even columns 0-10 as the wavelength data, and the odd columns 1-11 as the absobance data. 

import numpy
import os

def get_files(directory):
    items = os.listdir(directory)
    files = []
    for name in items:
        if name.endswith(".csv"):
            files.append(name)
    return files

def get_wavelength100x(filename):
	data = numpy.loadtxt(fname=filename)
	wavelengths = data[:,0]
	absorbances = data[:,1] #:13:2] #gets us all the absorbance columns for 100x
	return wavelengths, absorbances
def get_wavelength30x(filename):
        data = numpy.loadtxt(fname=filename)
        wavelengths = data[:,0]
        absorbances = data[:,3] 
        return wavelengths, absorbances
def get_wavelength10x(filename):
        data = numpy.loadtxt(fname=filename)
        wavelengths = data[:,0]
        absorbances = data[:,5] 
        return wavelengths, absorbances
def get_wavelength3x(filename):
        data = numpy.loadtxt(fname=filename)
        wavelengths = data[:,0]
        absorbances = data[:,7] 
        return wavelengths, absorbances
def get_wavelength1x(filename):
        data = numpy.loadtxt(fname=filename)
        wavelengths = data[:,0]
        absorbances = data[:,9] 
        return wavelengths, absorbances
def get_wavelength0x(filename):
        data = numpy.loadtxt(fname=filename)
        wavelengths = data[:,0]
        absorbances = data[:,11] 
        return wavelengths, absorbances







